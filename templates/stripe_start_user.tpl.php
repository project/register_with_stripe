<?php
/**
 * @file
 * Contains pay button form.
 */
?>
<div id="stripe-pay-button">
  <?php
   print drupal_render($charge_form);
  ?>
</div>
